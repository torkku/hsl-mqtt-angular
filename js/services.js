// listen to MQTT service for live updates
exports.HSLMQTTService = ['$rootScope', function($rootScope) {

	var service = {};

	service.connect = function(host, topic)  {
		console.log("HSLMQTTService: connect to host: " + host);

		var mqtt = require('mqtt');
		
		var client = mqtt.connect('mqtt://' + host + ":1883");

		client.on('connect', function() {
			console.log("HSLMQTTService: " + "subscribe for: " + topic);
			client.subscribe(topic);
		});

		client.on('message', function(topic, message) {
			$rootScope.$broadcast('vehicleJourneyMessageReceived', message);
			//client.end();
		});

	};

	return service;
}];

// load data from MQTT cache
exports.HSLMQTTCacheService = ['$http', function($http) {

	var service = {};

	service.getMQTTCacheData = function() {
		var promise = $http.get('http://dev.hsl.fi/hfp/');
		return promise;
	};

	return service;
}];
