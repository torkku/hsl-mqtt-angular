var directives = require('./directives');
var controllers = require('./controllers');
var services = require('./services');
var _ = require('underscore');

var components = angular.module('commuteApp.components', ['ng']);

_.each(directives, function(directive, name) {
	components.directive(name, directive);
});

_.each(controllers, function(controller, name) {
	components.controller(name, controller);
});

_.each(services, function(service, name) {
	components.service(name, service);
});

var app = angular.module('commuteApp', ['commuteApp.components', 'google.places', 'angularjs-dropdown-multiselect']);

/*
app.config(function($routeProvider) {
	$routeProvider
		.when('/', {
			template: '<vehicle-map></vehicle-map>'
		});
});
*/

