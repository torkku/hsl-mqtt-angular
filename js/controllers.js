var _ = require('underscore');

// XXX: move this to somewhere else
function getVehicleType(vehicleId) {
	var vehicleType;

	if (/^R/.test(vehicleId)) {
		vehicleType = 'Tram';
	} else if (/^\d/.test(vehicleId)) {
		vehicleType = 'Bus';
	} else if (/^H/.test(vehicleId)) {
		vehicleType = 'Train';
	} else {
		vehicleType = 'Bus';
	}

	return vehicleType;
}

// XXX: move this to somewhere else
function interpretJore(line) {

	// 'JORE' code handling
	// https://github.com/HSLdevcom/navigator-proto/blob/master/src/routing.coffee#L40
	var route;
	if (line.match(/^1019/) !== null) {
		// XXX: Ferry to do
		route = '%';
	} else if (line.match(/^1300/) !== null) {
		// subway
		route = line.substring(4,5);
	} else if (line.match(/^300/) !== null) {
		// train
		route = line.substring(4,5);
	} else if (line.match(/^10(0|10)/) !== null) {
		route = line.replace(/^.0*/, "");
	} else if (line.match(/^(1|2|4).../) !== null) {
		route = line.replace(/^.0*/, "");
	} else {
		// XXX: unknown
		route = line;
	}
	return route;
}


exports.AddressBarController = ['$scope', function($scope) {
	var lat, lng;
	var selectedPos = '';

	$scope.gotoCurrentLocation = function() {
		$scope.map.locate({ enableHighAccuracy: true });

		$scope.map.on('locationfound', function(pos) {

			// update our location on map
			var vehicleId = 'home';
			vehicleMap.home.setLatLng(pos.latlng);

			// pan map to location
			$scope.map.panTo(pos.latlng, { animate: true });
		});
	};

	$scope.updatePos = function(pos) {
		if ($scope.selectedPos.geometry !== undefined) {
			lat = pos.geometry.location.lat();
			lng = pos.geometry.location.lng();
			$scope.map.panTo([lat, lng], { animate: true });
		}
	};
}];

exports.RouteSelectController =
	['$scope',
	 'HSLMQTTCacheService',
	function($scope, HSLMQTTCacheService) {

		populateRouteSelection($scope.map, $scope.vehicleMap);

		function populateRouteSelection(map, vehicleMap) {
			// get promise for data from service
			var promise = HSLMQTTCacheService.getMQTTCacheData();

			// handle data
			promise.then(function(resp) {
				var journeys = resp.data;

				// collect routes here
				var options = [];

				// lookup hash
				var seen = {};

				// add each journey to select options
				_.each(journeys, function(journey) {
					var vp = journey.VP;
					var line = vp.line;
					var id = vp.veh;
					var route = interpretJore(vp.line);

					// no idea what these are, but they are no use
					if (/GMN/.test(route))
						return;

					if (seen[route] === undefined) {
						options.push({id: route, label: route, type: getVehicleType(id)});
						seen[route] = true;
					}
				});

				$scope.routeOptions = _.sortBy(options, function(entry) {
					var label = entry.label.replace(/[^0-9]/g, '');
					var pad = '00000';

					// numeric sort
					var ret = (pad+label).slice(-pad.length);

					// non numeric routes goes last
					if (ret == '00000')
						ret = '999';

					return ret;
				});
			});
		}

		$scope.cacheDataToMap = function() {
			var promise = HSLMQTTCacheService.getMQTTCacheData();

			// handle data
			promise.then(function(resp) {
				var journeys = resp.data;

				// remove all existing markers
				_.each($scope.vehicleMap, function(value, key) {
					$scope.map.removeLayer(value.marker);
					delete $scope.vehicleMap[key];
				});

				// add cached markers to map as necessary
				_.each(journeys, function(journey) {
					journeyToMap(journey);
				});
			});
		};

	}];

exports.VehicleMapController =
	['$scope',
	 '$timeout',
	 'HSLMQTTService', 
	 'HSLMQTTCacheService', 
	 function($scope, $timeout, HSLMQTTService, HSLMQTTCacheService) {

		// route selector settings
		$scope.selectSettings = {
			enableSearch: true
		};

/*
got message: {
  "desi": "10",
  "dir": "1",
  "oper": "XXX",
  "veh": "RHKL00074",
  "tst": "2016-01-10T18:42:12.000Z",
  "tsi": 1452451332,
  "spd": 2.88,
  "hdg": 322,
  "lat": 60.16814,
  "long": 24.941162,
  "dl": 32,
  "odo": 1026,
  "oday": "2016-01-10",
  "jrn": "XXX",
  "line": "XXX",
  "start": "2038",
  "stop_index": 5,
  "source": "hsl live"
}
to_mqtt_payload = (msg) ->
    VP:
        desi: msg.trip.route
        dir: msg.trip.direction
        oper: "XXX"
        veh: msg.vehicle.id
        tst: moment(msg.timestamp*1000).toISOString()
        tsi: Math.floor(msg.timestamp)
        spd: Math.round(msg.position.speed*100)/100
        hdg: msg.position.bearing
        lat: msg.position.latitude
        long: msg.position.longitude
        dl: msg.position.delay
        odo: msg.position.odometer
        oday: "XXX"
        jrn: "XXX"
        line: msg.trip.route
        start: msg.trip.start_time
        stop_index: msg.position.next_stop_index

*/

		var host = '213.138.147.225';
		var topic = '/hfp/journey/#';
		HSLMQTTService.connect(host, topic);

		$scope.$on('vehicleJourneyMessageReceived', function(event, message) {
			// handle received message
			var journey = JSON.parse(message.toString());
			//console.log("selectedRoutes: " + JSON.stringify($scope.selectedRoutes, null, 2));
			journeyToMap(journey);
		});

		function isRouteSelected(route) {
			// check if route is selected
			var bool = false;
			_.each($scope.selectedRoutes, function(elem) {
				if (elem.id == route)
					bool = true;
			});
			return bool;
		}

		function journeyToMap(json) {

			var map = $scope.map;
			var vehicleMap = $scope.vehicleMap;

			// get global map and vehicleMap from scope
			var vp = json.VP;

			// vehicle data
			var vehicleId = vp.veh;	
			var lat = vp.lat;
			var lng = vp.long;
			var heading = vp.hdg + 45;

			var route;
			if (vp.line == 'XXX') {
				route = vp.desi;
			} else {
				route = interpretJore(vp.line);
			}

			if (!$scope.selectedRoutes)
				return;

			if (!isRouteSelected(route))
				return;

			//console.log("message received: " + JSON.stringify(vp, null, 2));

			var vehicleType = getVehicleType(vehicleId);

			if (vehicleId in vehicleMap) {
				// get marker on map
				var vehicle = vehicleMap[vehicleId];

				// only update if this is a position recorded later
				if (vp.tsi > vehicle.timestamp) {
					// update vehicle on map
					var marker = vehicle.marker;

					marker.bindPopup(getPopupInfo(vp, route));

					var icon = getVehicleIcon(vehicleId, vehicleType, route, heading);
					marker.setIcon(icon);

					marker.setLatLng([lat, lng]);
				}
			} else {
				// create a new vehicle on map
				var newIcon = getVehicleIcon(vehicleId, vehicleType, route, heading);
				var newMarker = L.marker([lat, lng], {
					icon: newIcon
				});

				newMarker.bindPopup(getPopupInfo(vp, route));
				newMarker.addTo(map);
				vehicleMap[vehicleId] = {
					marker: newMarker,
					timestamp: vp.tsi
				};
			}
		}

		function getPopupInfo(vp, route) {
			var info = "<b>" + route + "</b><br/>" + vp.veh + "<br/>Start time: " + vp.start;
			if (vp.dl !== undefined)
				info += "<br/>" + getDelayString(vp.dl);

			var updated = new Date(vp.tsi * 1000).format('h:i:s');
			info += "<br/>Updated at: " + updated;

			return info;
		}

		function getDelayString(seconds) {
			require('datejs');

			var ret;
			if (seconds > 0)
				ret = "Delay: ";
			else
				ret = "Advance: ";

			ret += (new Date()).clearTime().addSeconds(Math.abs(seconds)).toString('HH:mm:ss');
			return ret;
		}

		function getVehicleIcon(vehicleId, vehicleType, line, heading) {
			var iconHtml = getVehicleIconHtml(vehicleId, vehicleType, line, heading);
			var icon = L.divIcon({
				html: iconHtml
			});

			return icon;
		}

		function getVehicleIconHtml(vehicleId, vehicleType, line, heading) {
			var textSpan, iconHtml;
			var classes = 'vehicleMarker' + ' ' + vehicleType;

			if (angular.isNumber(heading)) {
				textSpan = '<span style="display: block; transform:rotate(' + (-heading) + 'deg);">' + line + '</span>';
				iconHtml = '<div id="' + vehicleId + '" class="' + classes + '" style="display: block; transform:rotate(' + heading + 'deg);">' + textSpan + '</div>';
			} else {
				textSpan = '<span>' + line + '</span>';
				iconHtml = '<div id="' + vehicleId + '" class="' + classes + '">' + textSpan + '</div>';
			}
			return iconHtml;
		}

	}];
