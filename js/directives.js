exports.vehicleMap = function() {
	return {
		replace: true,
		controller: 'VehicleMapController',
		template: '<div></div>',
		link: function(scope, element, attrs) {
			// create new Leaflet map
			var map = new L.map(attrs.id);

			// leaflet params for OpenStreetMaps
			//var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
			var osmUrl = 'http://matka.hsl.fi/hsl-map/{z}/{x}/{y}.png';
			var osmAttrib ='Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
			var osm = new L.TileLayer(osmUrl, { minZoom: 12, maxZoom: 19, attribution: osmAttrib });
			map.addLayer(osm);

			var vehicleMap = {};
			scope.vehicleMap = vehicleMap;
			scope.map = map;

			map.locate({ enableHighAccuracy: true });

			map.on('locationfound', function(pos) {
				var home = L.marker(pos.latlng, {
					message: "This is me!"
				});
				home.addTo(map);

				// our location, home, is a specia vehicle
				var vehicleId = 'home';
				vehicleMap.vehicleId = {
					marker: home,
					timestamp: 0
				};

				map.setView(pos.latlng, 15);
			});

			map.on('locationerror', function(err) {
				// inform user
				alert("Could not get location. Using Pasila as default. Error: " + err.message);

				// start the map in Pasila, Helsinki
				var lat = 60.197;
				var lon = 24.922;
				map.setView([lat, lon], 15);
			});

			// this will hold selected routes
			scope.selectedRoutes = [];
		}
	};
};

