var gulp = require('gulp');
var jshint = require('gulp-jshint');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('gulp-buffer');
var uglify = require('gulp-uglify');

gulp.task('browserify', function() {
    return browserify({ entries: ['./js/index.js'] })
        .bundle()
        .pipe(source('index.js'))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(gulp.dest('./bin'));
});

gulp.task("jshint", function() {
     return gulp.src('./js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter());
});

gulp.task('watch', function() {
	gulp.watch(['./js/*.js'], ['jshint', 'browserify']);
});

